const express = require('express');
const bodyparser = require('body-parser');

const jsonParser = bodyparser.json();

const router = express.Router();

router.use(jsonParser);

router.get('/', async (req, res) => {

});

router.get('/:id', async (req, res) => {

});

router.get('/mail/:email', async (req, res) => {

});

router.put('/:id', async (req, res, next) => {

});

router.delete('/:id', async (req, res, next) => {

});

router.post('/', async (req, res, next) => {

});

router.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({error: "" + err});
    next();
})

module.exports = router;