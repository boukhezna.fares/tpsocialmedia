const {EventModel} = require("../schema/event");

const createUser = async ({userInfo}) => {
}

const findById = async ({id}) => {
}

const findByEmail = async ({email}) => {
}

const findAllUsers = async () => {
}

const updateOne = async ({userInfo, id}) => {
}
 
const deleteOne = async ({id}) => {
}

module.exports = {
    createUser,
    findAllUsers,
    findById,
    findByEmail,
    updateOne,
    deleteOne
}