const mongoose = require('mongoose');

const { findUserById } = require('../DTOs/users');

const EventSchema = new mongoose.Schema({
    name: {type: String, required: true},
    desc: {type: String, required: true},
    startDate: {type: Date, required: true},
    endDate: {type: Date, required: true},
    location: {type: String, required: true},
    coverImg: {type: String},
    isPrivate: { type: Boolean, required: true},
    userList: { type: [String] },
    adminList: { 
      type: [mongoose.Schema.Types.ObjectId], 
      required: true, 
      ref: 'User',
    },
});

const EventModel = new mongoose.model("Events", EventSchema);

module.exports = {EventSchema, EventModel};