const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    first: {type: String, required: true},
    last: {type: String, required: true},
    email: {type: String, required: true, unique: true},
});

const UserModel = new mongoose.model("Users", UserSchema);

module.exports = {UserModel, UserSchema};