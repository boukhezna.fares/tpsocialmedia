const express = require('express');
const bodyparser = require('body-parser');
const {
    createUser,
    findAllUsers,
    findUserById,
    findUserByEmail,
    updateOneUser,
    deleteOneUser
} = require('../DTOs/users');  

const jsonParser = bodyparser.json();

const router = express.Router();

router.use(jsonParser);

router.get('/', async (req, res) => {
    try{
        let users = await findAllUsers();
        res.status(200);
        res.send(users);
    }catch(e){
        next(e);
    }
});

router.get('/:id', async (req, res) => {
    
    let { id } = req.params;

    try{
        let user = await findUserById({id});
        res.status(200);
        res.send(user);
    }catch(e){
        next(e);
    }

});

router.get('/mail/:email', async (req, res) => {
    
    let { email } = req.params;

    try{
        let user = await findUserByEmail({email});
        res.status(200);
        res.send(user);
    }catch(e){
        next(e);
    }

});

router.put('/:id', async (req, res, next) => {

    let userInfo = req.body;
    let {id} = req.params;

    try{
        await updateOneUser({id, userInfo});
        res.status(200);
        res.send();
    }catch(e){
        next(e);
    }
    
});

router.delete('/:id', async (req, res, next) => {

    let {id} = req.params;

    try{
        await deleteOneUser({id});
        res.status(200);
        res.send();
    }catch(e){
        next(e);
    }

});

router.post('/', async (req, res, next) => {
    let userInfo = req.body;

    try{
        await createUser({userInfo});
        res.status(200);
        res.send();
    }catch(e){
        next(e);
    }
});

router.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({error: "" + err});
    next();
})

module.exports = router;