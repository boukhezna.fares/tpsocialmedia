const express = require('express');
const bodyparser = require('body-parser');
const { createEvent, findAllEvents } = require('../DTOs/events');

const jsonParser = bodyparser.json();

const router = express.Router();

router.use(jsonParser);

router.get('/', async (req, res) => {
    try{
        let events = await findAllEvents();
        res.status(200);
        res.send(events);
    }catch(e){
        next(e);
    }
});

router.get('/:id', async (req, res) => {

});

router.get('/mail/:email', async (req, res) => {

});

router.put('/:id', async (req, res, next) => {

});

router.delete('/:id', async (req, res, next) => {

});

router.post('/', async (req, res, next) => {

    let eventInfo = req.body;

    try{
        await createEvent({eventInfo});
        res.status(200);
        res.send();
    }catch(e){
        next(e)
    }
});

router.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({error: "" + err});
    next();
})

module.exports = router;