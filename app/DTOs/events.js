const {EventModel} = require("../schema/event");
const { findUserById } = require("../DTOs/users");

const createEvent = async ({eventInfo }) => {

    let event = new EventModel({...eventInfo});
    let errors = event.validateSync();

    if(errors)
        throw new Error(errors);

    if(eventInfo.adminList.length < 1)
        throw new Error("Event should at least have one admin");

    let existsError;

    for(let id of event.adminList){
        let user = await findById({id: id.toString()});

        if(!user)
            existsError = `user ${id} does not exist`;
    }

    for(let id of event.userList){
        let user = await findById({id: id.toString()});

        if(!user)
            existsError = `user ${id} does not exist`;
    }

    console.log(existsError);

    if(existsError)
        throw new Error(existsError);

    await event.save();
}

const findById = async ({id}) => {
}

const findAllEvents = async () => {
    return EventModel.find();
}

const updateOneEvent = async ({eventInfo, id}) => {
}
 
const deleteOneEvent = async ({id}) => {
}

const addUserToEvent = async({id}) => {
}

const addAdminToEvent = async({id}) => {

}

module.exports = {
    createEvent,
    findAllEvents,
    findById,
    updateOneEvent,
    deleteOneEvent,
    addUserToEvent,
    addAdminToEvent
}