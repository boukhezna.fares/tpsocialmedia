const {UserModel} = require("../schema/user");

const createUser = async ({userInfo}) => {
    let userInstance = new UserModel({ ...userInfo });
    let errors = userInstance.validateSync();

    if(errors)
        throw new Error(errors);

    let alreadyExists = await UserModel.findOne({email: userInfo.email });

    if(alreadyExists)
        throw new Error("Not unique");

    await userInstance.save();
}

const findUserById = async ({id}) => {
    return UserModel.findById(id);
}

const findUserByEmail = async ({email}) => {
    return UserModel.findOne({email: email});
}

const findAllUsers = async () => {
    return UserModel.find();
}

const updateOneUser = async ({userInfo, id}) => {
    return UserModel.findOneAndUpdate({_id: id}, { $set: userInfo });
}
 
const deleteOneUser = async ({id}) => {
    return UserModel.delete({_id: id});
}

module.exports = {
    createUser,
    findAllUsers,
    findUserById,
    findUserByEmail,
    updateOneUser,
    deleteOneUser
}