// Dépendencies
const express = require('express');
const mongoose = require('mongoose');

const uri = "mongodb://localhost:27017/tpsocialmedia";
const app = express();

const userRouter = require('./app/routes/users');
const eventRouter = require('./app/routes/event');

const main = async () => {
    
    await mongoose.connect(uri);

    app.get('/', (req, res) => {
        res.send("Hello world!"); 
    });

    app.use('/users', userRouter);
    app.use('/events', eventRouter);

    app.listen(3000, () => {
        console.log("listening on 3000...");
    });
}

main();
